<?php
// $Id: page.tpl.php,v 1.18 2010/12/02 11:42:42 danprobo Exp $
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
    <meta http-equiv="Content-Style-Type" content="text/css" />
  <?php print $styles; ?>
   <!--[if IE 6]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie6.css" type="text/css" /><![endif]-->
  <?php print $scripts; ?>
<!--[if IE 6]>
        <script type="text/javascript" src="<?php print $base_path . $directory; ?>/scripts/jquery.pngFix.js"></script>
<![endif]-->
<!--[if IE 6]>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(document).pngFix();
    });
</script>
<![endif]-->
<script type="text/javascript">
  jQuery(document).ready(function($) {
    $("#superfish ul.menu").superfish({ 
            delay:       100,                           
            animation:   {opacity:'show',height:'show'},  
            speed:       'fast',                          
            autoArrows:  true,                           
            dropShadows: true                   
        });
  });
</script>
 </head>

<body<?php print phptemplate_body_class($left, $right); ?>>
<div class="wrapall">
 <div id="tbar">
  <div id="tbar-wrapper">
   <div id="header">
    <div id="header-wrapper">
   <div id="header-first">
   <?php if ($logo): ?> 
    <div class="logo">
    <a href="<?php print $base_path ?>" title="<?php print $site_name ?>"><img src="<?php print $logo ?>" alt="<?php print $site_name ?>" /></a>
    </div>
   <?php endif; ?>
   </div><!-- /header-first -->
     <div id="header-middle">
     </div><!-- /header-middle -->
    </div><!-- /header-wrapper -->
   </div> <!-- /header -->
  </div> <!-- /tbar-wrapper -->
 </div> <!-- /tbar -->
<div style="clear:both"></div>

 <div id="mbar">
  <div id="menu">
  <?php if ($primary_links || $superfish_menu): ?>
  <!-- PRIMARY -->
   <div id="<?php print $primary_links ? 'nav' : 'superfish' ; ?>">
   <?php 
    		     if ($primary_links) {
		          print theme('links', $primary_links); 
    	      }
				      elseif (!empty($superfish_menu)) {
				        print $superfish_menu;
				      }
        ?>
   </div> <!-- /primary -->
  <?php endif; ?>
  </div> <!-- end /menu -->
 </div> <!-- end /mbar -->

 <?php if($preface_middle || $preface_last) : ?>
    <div style="clear:both"></div>
    <div id="preface-wrapper" class="in<?php print (bool) $preface_middle + (bool) $preface_last; ?>">
         <?php if($preface_middle) : ?>
          <div class="column B">
            <?php print $preface_middle; ?>
          </div>
          <?php endif; ?>
          <?php if($preface_last) : ?>
          <div class="column C">
            <?php print $preface_last; ?>
          </div>
          <?php endif; ?>
      <div style="clear:both"></div>
    </div>
    <?php endif; ?>

<div style="clear:both"></div>
<div id="wrapper">
 <?php if ($left): ?>
  <div id="sidebar-left" class="sidebar">
   <?php print $left ?>
  </div>
  <?php endif; ?>
  <div id="content">
   <?php if ($content_top) : ?>
   <div class="content-top"><?php print $content_top; ?></div>
   <?php endif; ?>

   <?php if ($show_messages) { print $messages; }; ?>
   <?php if ($tabs) : ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
   
   <?php print $help; ?>
   <?php if ($content) : ?>
   <div style="clear:both"></div>
   <div class="content-middle"><?php print $content; ?>
   </div>
  <?php endif; ?>
  <?php if ($content_bottom) : ?><div class="content-bottom"><?php print $content_bottom; ?>
  </div>
 <?php endif; ?>
 </div> <!-- end content -->

<?php if ($right): ?>
    <div id="sidebar-right" class="sidebar">
				<?php print $right; ?>
			</div>
		<?php endif; ?>
<div style="clear:both"></div>
</div> <!-- end wrapper -->

<?php if($bottom_first || $bottom_middle || $bottom_last) : ?>
    <div style="clear:both"></div>
    <div id="bottom-teaser" class="in<?php print (bool) $bottom_first + (bool) $bottom_middle + (bool) $bottom_last; ?>">
          <?php if($bottom_first) : ?>
          <div class="column A">
            <?php print $bottom_first; ?>
          </div>
          <?php endif; ?>
          <?php if($bottom_middle) : ?>
          <div class="column B">
            <?php print $bottom_middle; ?>
          </div>
          <?php endif; ?>
          <?php if($bottom_last) : ?>
          <div class="column C">
            <?php print $bottom_last; ?>
          </div>
          <?php endif; ?>
      <div style="clear:both"></div>
    </div>
    <?php endif; ?>

 <?php if($bottom_1 || $bottom_2 || $bottom_3 || $bottom_4) : ?>
    <div style="clear:both"></div><!-- Do not touch -->
    <div id="bottom-wrapper" class="in<?php print (bool) $bottom_1 + (bool) $bottom_2 + (bool) $bottom_3 + (bool) $bottom_4; ?>">
          <?php if($bottom_1) : ?>
          <div class="column A">
            <?php print $bottom_1; ?>
          </div>
          <?php endif; ?>
          <?php if($bottom_2) : ?>
          <div class="column B">
            <?php print $bottom_2; ?>
          </div>
          <?php endif; ?>
          <?php if($bottom_3) : ?>
          <div class="column C">
            <?php print $bottom_3; ?>
          </div>
          <?php endif; ?>
          <?php if($bottom_4) : ?>
          <div class="column D">
            <?php print $bottom_4; ?>
          </div>
          <?php endif; ?>
      <div style="clear:both"></div>
    </div><!-- Bottom -->
    <?php endif; ?>
    
  <div class="push"></div>
</div><!-- end /wrapall-->

<div style="clear:both"></div>
<div id="footer-wrapper">
  <div id="footer">
   <div class="footer-content">
    <div id="footer-message" class="block ">
    <div class="content"><?php print $footer_message; ?> </div>
   </div>
   <?php print $footer; ?>
   </div><!-- /footer-content -->
  </div><!-- /footer -->
</div> <!-- end footer wrapper -->

<div style="clear:both"></div>
<?php print $closure; ?>
</body>
</html>

